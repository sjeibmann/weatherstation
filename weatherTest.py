import pyowm
import RPi.GPIO as GPIO

# setup board output as BOARD or BCM
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

chanList = [5,6,13,19,26]
GPIO.setup(chanList, GPIO.OUT)

owm = pyowm.OWM('<include key here>')  # You MUST provide a valid API key

pdxWeather = owm.weather_at_place('Portland,Oregon')
w = pdxWeather.get_weather()
# print(w)
cond = w.get_temperature('fahrenheit')
print(cond['temp'])

if float(cond['temp']) <= 32:
        # print("condition: " + str(cond))
        print("freezing")
        GPIO.output(chanList, GPIO.LOW)
        GPIO.output(chanList[0], GPIO.HIGH)
elif float(cond['temp']) > 32 and float(cond['temp']) <= 55:
        # print("condition: " + str(cond))
        print("cold")
        GPIO.output(chanList, GPIO.LOW)
        GPIO.output(chanList[1], GPIO.HIGH)
elif float(cond['temp']) > 55 and float(cond['temp']) <= 68:
        # print("condition: " + str(cond))
        print("meh")
        GPIO.output(chanList, GPIO.LOW)
        GPIO.output(chanList[2], GPIO.HIGH)
elif float(cond['temp']) > 68 and float(cond['temp']) <= 78:
        # print("condition: " + str(cond))
        print("warm")
        GPIO.output(chanList, GPIO.LOW)
        GPIO.output(chanList[3], GPIO.HIGH)
elif float(cond['temp']) > 78:
        # print("condition: " + str(cond))`
        print("hot")
        GPIO.output(chanList, GPIO.LOW)
        GPIO.output(chanList[4], GPIO.HIGH)
elif cond is None:
        print("no weather available")
        GPIO.output(chanList, GPIO.LOW)

