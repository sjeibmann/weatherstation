# README #

Overview of version 0.1 of the desktop weatherStation.

### What is this repository for? ###

* Simple weather station for Raspberry Pi, that fetches data from OpenWeatherMaps and expresses the temperature in one of five simple statements.

### How do I get set up? ###

* You will need a Raspberry Pi 2 (B+) running Rasbian Jessie and some LED, wire and female headers.
* Begin by going to http://openweathermap.org/ and creating an account
* install OpenWeatherMaps API on your Raspberry PI
```
#!shell

pip install pyowm
```
 additionally you will need to install RPi.GPIO or make sure it is up to date.

```
#!shell

sudo apt-get install python-dev python-rpi.gpio
```
* Edit the weatherTest.py script and include your OpenWeatherMap Key where is says '<include key here>'

```
#!python

owm = pyowm.OWM('<include key here>')  # You MUST provide a valid API key

```
* Edit the getWeather.sh script to point to the correct path to weatherTest.py

```
#!bash

#!/bin/sh
echo running weather script
cd python_scripts/  # edit this line with correct path
python weatherTest.py
```

* Edit the crontab to check the weather every 5 minutes using:
```
#!shell

crontab -e
```
add a new line at the end of the crontab file that looks like this

```
#!shell

*/5 * * * * bash /path/to/script.sh

```
make sure to replace "/path/to/script.sh" with the explicit path to your getWeather.sh script

### Whats Next? ###

* Add word clock funtionality... needs more LED's
* add localized weather info using a weather sheild